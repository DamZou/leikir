require 'rails_helper'

RSpec.describe "adresses/new", type: :view do
  before(:each) do
    assign(:adress, Adress.new(
      :number => 1,
      :number_type => "MyString",
      :street => "MyString",
      :city => "MyString",
      :zip_code => "MyString",
      :country => "MyString",
      :contact_id => 1,
      :apartment => "MyString"
    ))
  end

  it "renders new adress form" do
    render

    assert_select "form[action=?][method=?]", adresses_path, "post" do

      assert_select "input#adress_number[name=?]", "adress[number]"

      assert_select "input#adress_number_type[name=?]", "adress[number_type]"

      assert_select "input#adress_street[name=?]", "adress[street]"

      assert_select "input#adress_city[name=?]", "adress[city]"

      assert_select "input#adress_zip_code[name=?]", "adress[zip_code]"

      assert_select "input#adress_country[name=?]", "adress[country]"

      assert_select "input#adress_contact_id[name=?]", "adress[contact_id]"

      assert_select "input#adress_apartment[name=?]", "adress[apartment]"
    end
  end
end
