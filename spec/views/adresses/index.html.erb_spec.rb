require 'rails_helper'

RSpec.describe "adresses/index", type: :view do
  before(:each) do
    assign(:adresses, [
      Adress.create!(
        :number => 2,
        :number_type => "Number Type",
        :street => "Street",
        :city => "City",
        :zip_code => "Zip Code",
        :country => "Country",
        :contact_id => 3,
        :apartment => "Apartment"
      ),
      Adress.create!(
        :number => 2,
        :number_type => "Number Type",
        :street => "Street",
        :city => "City",
        :zip_code => "Zip Code",
        :country => "Country",
        :contact_id => 3,
        :apartment => "Apartment"
      )
    ])
  end

  it "renders a list of adresses" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Number Type".to_s, :count => 2
    assert_select "tr>td", :text => "Street".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Zip Code".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Apartment".to_s, :count => 2
  end
end
