require 'rails_helper'

RSpec.describe "adresses/show", type: :view do
  before(:each) do
    @adress = assign(:adress, Adress.create!(
      :number => 2,
      :number_type => "Number Type",
      :street => "Street",
      :city => "City",
      :zip_code => "Zip Code",
      :country => "Country",
      :contact_id => 3,
      :apartment => "Apartment"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Number Type/)
    expect(rendered).to match(/Street/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Zip Code/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Apartment/)
  end
end
