require 'rails_helper'

RSpec.describe "tags/new", type: :view do
  before(:each) do
    assign(:tag, Tag.new(
      :title => "MyString",
      :contact_id => 1
    ))
  end

  it "renders new tag form" do
    render

    assert_select "form[action=?][method=?]", tags_path, "post" do

      assert_select "input#tag_title[name=?]", "tag[title]"

      assert_select "input#tag_contact_id[name=?]", "tag[contact_id]"
    end
  end
end
