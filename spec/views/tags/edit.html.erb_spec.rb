require 'rails_helper'

RSpec.describe "tags/edit", type: :view do
  before(:each) do
    @tag = assign(:tag, Tag.create!(
      :title => "MyString",
      :contact_id => 1
    ))
  end

  it "renders the edit tag form" do
    render

    assert_select "form[action=?][method=?]", tag_path(@tag), "post" do

      assert_select "input#tag_title[name=?]", "tag[title]"

      assert_select "input#tag_contact_id[name=?]", "tag[contact_id]"
    end
  end
end
