require 'rails_helper'

RSpec.describe "telephones/new", type: :view do
  before(:each) do
    assign(:telephone, Telephone.new(
      :title => "MyString",
      :number => "MyString",
      :contact_id => 1
    ))
  end

  it "renders new telephone form" do
    render

    assert_select "form[action=?][method=?]", telephones_path, "post" do

      assert_select "input#telephone_title[name=?]", "telephone[title]"

      assert_select "input#telephone_number[name=?]", "telephone[number]"

      assert_select "input#telephone_contact_id[name=?]", "telephone[contact_id]"
    end
  end
end
