require 'rails_helper'

RSpec.describe "telephones/show", type: :view do
  before(:each) do
    @telephone = assign(:telephone, Telephone.create!(
      :title => "Title",
      :number => "Number",
      :contact_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Number/)
    expect(rendered).to match(/2/)
  end
end
