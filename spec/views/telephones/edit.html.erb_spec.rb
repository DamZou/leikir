require 'rails_helper'

RSpec.describe "telephones/edit", type: :view do
  before(:each) do
    @telephone = assign(:telephone, Telephone.create!(
      :title => "MyString",
      :number => "MyString",
      :contact_id => 1
    ))
  end

  it "renders the edit telephone form" do
    render

    assert_select "form[action=?][method=?]", telephone_path(@telephone), "post" do

      assert_select "input#telephone_title[name=?]", "telephone[title]"

      assert_select "input#telephone_number[name=?]", "telephone[number]"

      assert_select "input#telephone_contact_id[name=?]", "telephone[contact_id]"
    end
  end
end
