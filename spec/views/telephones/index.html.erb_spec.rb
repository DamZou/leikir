require 'rails_helper'

RSpec.describe "telephones/index", type: :view do
  before(:each) do
    assign(:telephones, [
      Telephone.create!(
        :title => "Title",
        :number => "Number",
        :contact_id => 2
      ),
      Telephone.create!(
        :title => "Title",
        :number => "Number",
        :contact_id => 2
      )
    ])
  end

  it "renders a list of telephones" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Number".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
