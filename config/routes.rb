Rails.application.routes.draw do

  root "contacts#index"
  resources :contacts do
    resources :tags, except: [:index, :show]
    resources :adresses, except: [:index, :show]
    resources :telephones, except: [:index, :show]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
