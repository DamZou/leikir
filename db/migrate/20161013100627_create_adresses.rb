class CreateAdresses < ActiveRecord::Migration[5.0]
  def change
    create_table :adresses do |t|
      t.integer :number
      t.string :number_type
      t.string :street
      t.string :city
      t.string :title
      t.string :zip_code
      t.string :country
      t.integer :contact_id
      t.string :apartment

      t.timestamps
    end
  end
end
