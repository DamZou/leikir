class CreateTelephones < ActiveRecord::Migration[5.0]
  def change
    create_table :telephones do |t|
      t.string :title
      t.string :number
      t.integer :contact_id

      t.timestamps
    end
  end
end
