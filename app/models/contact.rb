class Contact < ApplicationRecord

	has_many :telephones
	has_many :tags
	has_many :adresses

	mount_uploader :image, ImageUploader

end
