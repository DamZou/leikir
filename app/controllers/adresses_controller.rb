class AdressesController < ApplicationController
  before_action :set_adress, only: [:edit, :update, :destroy]


  # GET /adresses/new
  def new
    @adress = Adress.new
  end

  # GET /adresses/1/edit
  def edit
  end

  # POST /adresses
  # POST /adresses.json
  def create
    @adress = Adress.new(adress_params)
    @adress.contact_id = params[:contact_id]
    @adress.save
  end

  # PATCH/PUT /adresses/1
  # PATCH/PUT /adresses/1.json
  def update
    @adress.update(adress_params)
  end

  # DELETE /adresses/1
  # DELETE /adresses/1.json
  def destroy
    @adress.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adress
      @adress = Adress.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adress_params
      params.require(:adress).permit(:number, :number_type, :street, :city, :zip_code, :country, :contact_id, :apartment, :title)
    end
end
