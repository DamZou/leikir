class TelephonesController < ApplicationController
  before_action :set_telephone, only: [:edit, :update, :destroy]


  # GET /telephones/new
  def new
    @telephone = Telephone.new
  end

  # GET /telephones/1/edit
  def edit
  end

  # POST /telephones
  # POST /telephones.json
  def create
    @telephone = Telephone.new(telephone_params)
    @telephone.contact_id = params[:contact_id]
    @telephone.save
  end

  # PATCH/PUT /telephones/1
  # PATCH/PUT /telephones/1.json
  def update
    @telephone.update(telephone_params)
  end

  # DELETE /telephones/1
  # DELETE /telephones/1.json
  def destroy
    @telephone.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_telephone
      @telephone = Telephone.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def telephone_params
      params.require(:telephone).permit(:title, :number, :contact_id)
    end
end
