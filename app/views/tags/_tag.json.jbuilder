json.extract! tag, :id, :title, :contact_id, :created_at, :updated_at
json.url tag_url(tag, format: :json)