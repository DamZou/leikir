json.extract! telephone, :id, :title, :number, :contact_id, :created_at, :updated_at
json.url telephone_url(telephone, format: :json)