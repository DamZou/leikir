json.extract! adress, :id, :number, :number_type, :street, :city, :zip_code, :country, :contact_id, :apartment, :created_at, :updated_at
json.url adress_url(adress, format: :json)